const { createHash } = require('crypto');
const { mkdir, open, opendir, readFile, writeFile, lstat, exists } = require('fs/promises');
const { mermaidToSvg } = require('./mmd-lib.js');

const DEST_FOLDER = 'gen';
const MMD_FOLDER = `${DEST_FOLDER}/${process.env.MMD_FOLDER || 'mmd'}`;
const MMD_FILE_HASH_SALT = process.env.MMD_FILE_HASH_SALT || false;

async function mkdirIfNotExists(dirname) {
  try {
    const stats = await lstat(dirname);
    // if (!stats.isDirectory()) throw `${dirname} exists but is no directory`;  
  } catch (e) {
    await mkdir(dirname);
  }
}

(async () => { 
  try {     
    await mkdirIfNotExists(DEST_FOLDER);
    await mkdirIfNotExists(MMD_FOLDER);

    const dir = await opendir('./');
    for await (const dirent of dir) {
      if (dirent.isFile() && dirent.name.substring(dirent.name.length - 3) === 'mmd') {
      
        let saltedHash = '';
        if (!!MMD_FILE_HASH_SALT) {
          const saltedFilename =`${MMD_FILE_HASH_SALT}${dirent.name}`;
          saltedHash = '-' + createHash("sha256").update(saltedFilename).digest("hex"); 
        } 

        const baseFilename = dirent.name.substring(0, dirent.name.length - 4);
        const svgWithSaltedHash = `${baseFilename}${saltedHash}.svg`;
        const svgPath = `${MMD_FOLDER}/${svgWithSaltedHash}`;

        const mmd = await readFile(dirent.name);
        const svg = await mermaidToSvg(mmd);        
        await writeFile(svgPath, svg);
      }
    }

  } catch (err) {
    console.error(err);
  }
})();

