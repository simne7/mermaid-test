const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { readFile, writeFile } = require('fs/promises');
const { mermaidToSvg } = require('./mmd-lib.js');

const argv = yargs(hideBin(process.argv))
  .option({
    'in': {
      alias: 'i',
      describe: 'input file',
      demandOption: true
    },
    'out': {
      alias: 'o',
      describe: 'output file (default: input + ".svg")'
    }
  })
  .help('help')
  .argv

if (!argv.o) {
  argv.o = argv.i + '.svg';
  argv.out = argv.o;
}

(async () => {
  try {      
    const mmd = await readFile(argv.i);
    const svg = await mermaidToSvg(mmd);
    await writeFile(argv.o, svg);
  } catch (err) {
    console.error(err);
  }
})();
