const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const https = require('https');

function mermaidToBase64(mmd) {
  const conf = {
    code: mmd + ''
  };
  const str = JSON.stringify(conf);
  return Buffer.from(str, 'binary').toString('base64');
}

async function downloadSvg(url) {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      let rawData = '';
      res.on('data', (chunk) => { rawData += chunk; });
      res.on('end', () => { resolve(rawData); });
    }).on('error', (e) => {
      reject(e);
    });
  });
}

async function mermaidToSvg(source_code) {
  const b64 = mermaidToBase64(source_code);    
  const url = `https://mermaid.ink/svg/${b64}`;
  return await downloadSvg(url); 
}

module.exports = {
  mermaidToSvg
};
