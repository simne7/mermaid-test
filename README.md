# Mermaid-Test

Demo project to show how Gitlab Pages can be used to host Mermaid diagram SVGs.

1. Upon change CI sends Mermaid code from `test.mmd` to https://mermaid.ink/svg/... 
2. CI downloads resulting SVG and stores it
3. CI publishes downloaded SVG on Gitlab Pages
4. SVG is available at https://simne7.gitlab.io/mmd/mermaid-test/test.svg
5. Use it in markdown `![TestSVG](https://simne7.gitlab.io/mermaid-test/mmd/test.svg)` or embed the link

---

![TestSVG](https://simne7.gitlab.io/mermaid-test/mmd/test.svg)


## Hard to guess URLs

Use the environment variables mentioned below to publish the SVGs under hard to guess URLs.

1. Set `MMD_FOLDER` to a random value, e.g. `fd049`
1. Set `MMD_FILE_HASH_SALT` to a random value, e.g. `5419cd76e621e284c1ead930b3d0730297e6929ac8f53abb0150051008c8abea`

Filenames (e.g. `test.mmd`) will be prepended with the salt and then hashed: `sha256(MMD_FILE_HASH_SALT + 'test.mmd')`. 

The resulting hash is appended to the original filename (without the `.mmd` suffix): `test-{HASH}.svg`.

Final files are then stored in the `MMD_FOLDER` directory.

The resulting URL looks like https://simne7.gitlab.io/mmd/mermaid-test/{MMD_FOLDER}/test-{HASH}.svg.

## Environment variables

You may configure the build pipeline using the following variables:

* `MMD_FOLDER` - Sets the folder name where downloaded SVGs are stored. Defaults to `mmd`. 
* `MMD_FILE_HASH_SALT` - Sets a salt for hashing the SVG filenames which makes them hard to guess. Feature is only enabled if variable is set.
